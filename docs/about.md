Sở Giao dịch Hàng hóa Việt Nam (Mercantile Exchange of Vietnam – MXV) là đơn vị tổ chức thị trường giao dịch hàng hóa phái sinh cấp quốc gia duy nhất hiện nay tại Việt Nam được Bộ Công Thương cấp phép.

Ngày 28/12/2006, Thủ tướng Chính phủ ban hành Nghị định số 158/2006/NĐ-CP quy định chi tiết Luật Thương mại về việc thành lập Sở Giao dịch Hàng hóa và hoạt động mua bán hàng hoá qua Sở Giao dịch Hàng hóa.

Ngày 01/09/2010, Bộ Công Thương cấp giấy phép số 4596/GP-BCT thành lập Sở Giao dịch Hàng hoá đầu tiên tại Việt Nam – Vietnam Commodity Exchange (MXV) (DBA: VNX).Theo giấy phép này, Bộ Công Thương cho phép MXV thực hiện các giao dịch cà phê, cao su, thép.

Ngày 09/04/2018, Thủ tướng Chính phủ ban hành Nghị định số 51/2018/NĐ-CP về việc sửa đổi, bổ sung một số điều của Nghị định 158/2006/NĐ-CP quy định chi tiết Luật thương mại về hoạt động mua bán hàng hóa qua Sở Giao dịch Hàng hóa.

Ngày 08/06/2018, Bộ Công Thương cấp giấy phép số 486/GP-BCT thành lập Sở Giao dịch Hàng hóa, cho phép sử dụng tên chính thức giao dịch trong nước: Sở Giao dịch Hàng hóa Việt Nam và tên giao dịch quốc tế: Mercantile Exchange of Vietnam (MXV).

Ngày 18/06/2018, MXV nộp hồ sơ giao dịch các hàng hóa được phép giao dịch liên thông và được Bộ Công Thương chấp thuận theo các nguyên tắc của Nghị định 51/2018/NĐ-CP.

Ngày 20/06/2018, MXV hoàn thành đăng ký danh sách Legal Entity Identifier (LEI), là mã định danh pháp nhân bao gồm 20 ký tự chữ và số, được sử dụng trên toàn thế giới, nhằm mục đích định danh riêng biệt các pháp nhân tham gia vào các giao dịch tài chính.

Ngày 22/05/2020, Bộ Công Thương ký Quyết định số 1369/QĐ-BCT cho phép Sở Giao dịch Hàng hóa Việt Nam niêm yết giao dịch một số mặt hàng thuộc nhóm kinh doanh có điều kiện (mặt hàng thuộc nhóm năng lượng và mặt hàng gạo). Sau khi được Bộ Công Thương cho phép, MXV đã tổ chức niêm yết giao dịch các sản phẩm Xăng pha chế RBOB, Khí tự nhiên, Dầu WTI, Dầu WTI mini, Dầu thô Brent, Dầu ít lưu huỳnh và Gạo thô.

Tháng 6/2020, MXV hoàn thành việc tăng vốn điều lệ 500 tỷ đồng, từng bước khẳng định vị thế để vươn lên trở thành một Sở Giao dịch hàng hóa tầm cỡ trong khu vực.

Tháng 7/2020, MXV đã đưa hệ thống phần mềm giao dịch CQG vào hoạt động thay thế cho hệ thống phần mềm Vision Commodity trước đó. CQG là hệ thống chuyển lệnh và lưu trữ dữ liệu do Công ty Công nghệ đa quốc gia CQG cung cấp. Đây là hệ thống phần mềm giao dịch uy tín nhất thế giới kết nối hơn 40 Sở Giao dịch Hàng hóa, đảm bảo đường truyền dữ liệu và có khả năng lưu trữ dữ liệu giao dịch lớn nhất.

Tháng 1/2021, MXV đã đưa vào vận hành hệ thống M-System. Đây là hệ thống phần mềm quản trị giao dịch với giao diện thân thiện, đáp ứng mọi nhu cầu giao dịch của nhà đầu tư.

Trong khoảng thời gian từ tháng 06/2021 đến tháng 7/2021, MXV đã bổ sung nhiều sản phẩm mới, phục vụ việc đa dạng hóa danh mục, hình thức đầu tư, cụ thể: (i) Bổ sung hình thức giao dịch chênh lệch giá Spread, đây là hình thức giao dịch phổ biến trên thế giới, đóng vai trò quan trọng trong cả hoạt động bảo hiểm giá (hedging) và đầu tư; (ii) Niêm yết giao dịch hai sản phẩm mới là Gạo thô (ZRE) và Lúa mì Kansas (KWE); (iii) Kết nối liên thông với Sở Giao dịch Kim loại London (London Metal Exchange - LME) - Sở Giao dịch lớn và lâu đời nhất trên thế giới đối với các mặt hàng kim loại.

Hiện nay, MXV đã liên thông hầu hết các Sở Giao dịch Hàng hóa lớn trên thế giới như: Sở Giao dịch Kim loại London - London Metal Exchange (LME); Sở Giao dịch Hàng hóa Chicago - CME Group (bao gồm các Sàn giao dịch CBOT, CME, COMEX, NYMEX); Sở Giao dịch liên lục địa - ICE (bao gồm các sàn giao dịch ICE US, ICE EU, ICE Singapore); Sở Giao dịch Hàng hóa Osaka Exchange - OSE; Sở Giao dịch Hàng hóa Singapore - SGX; Sở Giao dịch Phái sinh Bursa Malaysia Derivatives.

Sở Giao dịch Hàng hóa Việt Nam đáp ứng toàn bộ nhu cầu giao dịch hàng hóa bao gồm bảo hiểm rủi ro về giá và giao dịch chênh lệch giá của các nhà đầu tư trong nước, từng bước mở rộng quy mô giao dịch tập trung của trị trường hàng hóa Việt Nam, góp phần vào quá trình hội nhập kinh tế quốc tế.
