# MXV - Báo Công Thương API

---

- Địa chỉ truy vấn api `https://exapi.mxv.com.vn:4589`

---

## I/ Đăng ký IP

- Hệ thống chỉ trả về dữ liệu với những IP được đăng ký trên MXV.
- Những truy cập từ IP khác sẽ bị từ chối.

## II/ API

## `1. Lấy dữ liệu lịch sử giá thanh toán (GetSettle)`

- Hệ thống mặc định luôn trả về hợp đồng tháng Frontmonth, sản phẩm demo: Dầu WTI, Dầu Brent, Dầu ít lưu huỳnh, Khí tự nhiên, Xăng RBOB

Tham số:

| Tên tham số     | Kiểu dữ liệu | Bắc buộc | Diễn giải                                                  |
| :-------------- | :----------- | :------- | :--------------------------------------------------------- |
| `output_format` | string       | no       | Định dạng dữ liệu trả về (`xml`, `json`) - mặc định là xml |

Dữ liệu trả về (Dữ liệu trả về theo 2 định dạng xml hoặc json với những thuộc tính sau)

- Dữ liệu chung

  | Tên tham số | Diễn giải                                           |
  | :---------- | :-------------------------------------------------- |
  | `Date`      | Ngày phiên đóng cửa gần nhất (định dạng yyyy-mm-dd) |
  | `PrevDate`  | Ngày phiên đóng cửa trước đó (định dạng yyyy-mm-dd) |

- Dữ liệu hợp đồng

  | Tên tham số     | Diễn giải                                                                 |
  | :-------------- | :------------------------------------------------------------------------ |
  | `Symbol`        | Tên hàng hóa                                                              |
  | `Contract`      | Mã hợp đồng                                                               |
  | `ContractName`  | Tên hợp đồng                                                              |
  | `ContractMonth` | Tháng đáo hạn hợp đồng                                                    |
  | `Currency`      | Tiền tệ                                                                   |
  | `ContractUnit`  | Đơn vị hợp đồng                                                           |
  | `SettlePrice`   | Giá thanh toán                                                            |
  | `ChangePct`     | Phần trăm thay đổi giá thanh toán của hợp đồng so với ngày phiên trước đó |

```plaintext
1.1 [GET] Example:
```

- Endpoint: `https://exapi.mxv.com.vn:4589/GetSettle?output_format=xml`
- Example response:

```xml
<Contracts Date="2022-10-26" PrevDate="2022-10-25">
	<Item>
		<Contract>CLEZ22</Contract>
		<SettlePrice>87.91</SettlePrice>
		<ContractMonth>12/22</ContractMonth>
		<ContractName>Dầu WTI 12/22</ContractName>
		<ChangePct>3.04</ChangePct>
		<Symbol>Dầu WTI</Symbol>
		<Currency>USD</Currency>
		<ContractUnit>Barrels</ContractUnit>
	</Item>
	<Item>
		<Contract>NGEX22</Contract>
		<SettlePrice>5.606</SettlePrice>
		<ContractMonth>11/22</ContractMonth>
		<ContractName>Khí tự nhiên 11/22</ContractName>
		<ChangePct>-0.12</ChangePct>
		<Symbol>Khí tự nhiên</Symbol>
		<Currency>USD</Currency>
		<ContractUnit>MM BTU</ContractUnit>
	</Item>
	<Item>
		<Contract>QOZ22</Contract>
		<SettlePrice>95.69</SettlePrice>
		<ContractMonth>12/22</ContractMonth>
		<ContractName>Dầu Brent 12/22</ContractName>
		<ChangePct>2.32</ChangePct>
		<Symbol>Dầu Brent</Symbol>
		<Currency>USD</Currency>
		<ContractUnit>Barrels</ContractUnit>
	</Item>
	<Item>
		<Contract>QPX22</Contract>
		<SettlePrice>1105.25</SettlePrice>
		<ContractMonth>11/22</ContractMonth>
		<ContractName>Dầu ít lưu huỳnh 11/22</ContractName>
		<ChangePct>3.08</ChangePct>
		<Symbol>Dầu ít lưu huỳnh</Symbol>
		<Currency>USD</Currency>
		<ContractUnit>Tonnes</ContractUnit>
	</Item>
	<Item>
		<Contract>RBEX22</Contract>
		<SettlePrice>2.8994</SettlePrice>
		<ContractMonth>11/22</ContractMonth>
		<ContractName>Xăng RBOB 11/22</ContractName>
		<ChangePct>-0.57</ChangePct>
		<Symbol>Xăng RBOB</Symbol>
		<Currency>USD</Currency>
		<ContractUnit>gallons</ContractUnit>
	</Item>
</Contracts>
```

```plaintext
1.2 [POST] Example:
```

- Endpoint: `https://exapi.mxv.com.vn:4589/GetSettle`

- Example request: (json format)

```json
{
  "output_format": "json"
}
```

- Example response:

```json
{
  "data": [
    {
      "ChangePct": 3.04,
      "Contract": "CLEZ22",
      "ContractMonth": "12/22",
      "ContractName": "D\u1ea7u WTI 12/22",
      "ContractUnit": "Barrels",
      "Currency": "USD",
      "SettlePrice": 87.91,
      "Symbol": "D\u1ea7u WTI"
    },
    {
      "ChangePct": -0.12,
      "Contract": "NGEX22",
      "ContractMonth": "11/22",
      "ContractName": "Kh\u00ed t\u1ef1 nhi\u00ean 11/22",
      "ContractUnit": "MM BTU",
      "Currency": "USD",
      "SettlePrice": 5.606,
      "Symbol": "Kh\u00ed t\u1ef1 nhi\u00ean"
    },
    {
      "ChangePct": 2.32,
      "Contract": "QOZ22",
      "ContractMonth": "12/22",
      "ContractName": "D\u1ea7u Brent 12/22",
      "ContractUnit": "Barrels",
      "Currency": "USD",
      "SettlePrice": 95.69,
      "Symbol": "D\u1ea7u Brent"
    },
    {
      "ChangePct": 3.08,
      "Contract": "QPX22",
      "ContractMonth": "11/22",
      "ContractName": "D\u1ea7u \u00edt l\u01b0u hu\u1ef3nh 11/22",
      "ContractUnit": "Tonnes",
      "Currency": "USD",
      "SettlePrice": 1105.25,
      "Symbol": "D\u1ea7u \u00edt l\u01b0u hu\u1ef3nh"
    },
    {
      "ChangePct": -0.57,
      "Contract": "RBEX22",
      "ContractMonth": "11/22",
      "ContractName": "X\u0103ng RBOB 11/22",
      "ContractUnit": "gallons",
      "Currency": "USD",
      "SettlePrice": 2.8994,
      "Symbol": "X\u0103ng RBOB"
    }
  ],
  "date": "2022-10-26",
  "prevDate": "2022-10-25"
}
```
